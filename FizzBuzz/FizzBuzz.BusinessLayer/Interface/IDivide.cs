﻿namespace FizzBuzz.BusinessLayer.Interface
{
    public interface IDivide
    {
        bool IsDivisible(int Value);
        string FizzBuzzLiteral();
    }
}
