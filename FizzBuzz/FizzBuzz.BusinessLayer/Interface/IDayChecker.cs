﻿namespace FizzBuzz.BusinessLayer.Interface
{
    public interface IDayChecker
    {
        bool IsWednesday(string currentDay);
    }
}
