﻿using FizzBuzz.BusinessLayer.Interface;

namespace FizzBuzz.BusinessLayer.Rules
{
    public class DayChecker : IDayChecker
    {
        public bool IsWednesday(string currentDay)
        {
            return currentDay == Constants.SpecifiedDay;
        }
    }
}