﻿using FizzBuzz.BusinessLayer.Interface;
using System.Collections.Generic;
using System.Linq;

namespace FizzBuzz.BusinessLayer.Rules
{
    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly IEnumerable<IDivide> divisionList;

        public FizzBuzzService(IEnumerable<IDivide> divisionList)
        {
            this.divisionList = divisionList;
        }

        public IEnumerable<string> FizzBuzzList(int number)
        {
            var fizzBuzzList = new List<string>();
            for (int index = 1; index <= number; index++)
            {
                var isDivisible = this.divisionList.Where(m => m.IsDivisible(index)).ToList();
                fizzBuzzList.Add(isDivisible.Any() ? string.Join(" ", isDivisible.Select(m => m.FizzBuzzLiteral())) : index.ToString());
            }

            return fizzBuzzList;
        }
    }
}